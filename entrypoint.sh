#!/bin/bash

# exit when a command fails
set -e
# exit when script tries to use undeclared variables
set -u

args="$*"

# https://github.com/ufoscout/docker-compose-wait
/wait

echo "Bootstrap initiated, checking environmental variables."

# psql has certain environmental variables it uses, that I am mapping here
# I want to keep my own environmental variables aligned across microservices
export PGUSER=$POSTGRESQL_USER
export PGPASSWORD=$POSTGRESQL_PASSWORD
export PGHOST=$POSTGRESQL_HOST
export PGPORT=$POSTGRESQL_PORT
export PGDATABASE=$POSTGRESQL_DATABASE

echo "Bootstrapping data with arguments: $args"

exec psql $args
