SELECT pg_catalog.setval('public.resort_id_seq', 6, true);

COPY public.resort (id, continent, country, village, lat, lng, altitude_min_m, altitude_max_m, slopes_total_km, slopes_blue_km, slopes_red_km, slopes_black_km, lifts) FROM stdin;
1	Europe	Switzerland	Adelboden/​Lenk – Chuenisbärgli/​Silleren/​Hahnenmoos/​Metsch	46.4530559999999966	7.49500000000000011	940	1260	86	46	34	6	23
2	Europe	Austria	Spieljoch – Fügen	47.3389657000000028	11.8432809999999993	1404	650	171	36	108	27	8
3	Asia	Japan	Akan Royal Valley	36.2048240000000021	138.252924000000007	80	20	1	5	5	0	1
4	North America	Canada	Mont Christie	56.1303660000000022	-106.346771000000004	\N	\N	\N	\N	\N	\N	\N
5	Asia	Pakistan	Nathiagali	34.0729404000000002	73.3811848999999938	\N	\N	\N	\N	\N	\N	\N
\.
