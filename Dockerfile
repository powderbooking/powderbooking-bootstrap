FROM alpine:3.20

ENV WAIT_VERSION=2.12.1

# https://github.com/ufoscout/docker-compose-wait
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/${WAIT_VERSION}/wait /wait

RUN apk add --no-cache postgresql-client bash && \
    chmod +x /wait

COPY . /app

RUN chmod +x /app/entrypoint.sh && \
    # Create a non-root user
    adduser -D web && \
    chown web /app

USER web

WORKDIR /app/data

ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["-f", "resort_data.sql"]